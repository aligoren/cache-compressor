#Cache Compressor

Cache Compressor written with Python. Compressor method => `gzip`

#Usage:

**For current directory:**

~~~~{.shell}
johndoe@linux $ cc.py -c FILENAME

// output current directory => FILENAME.tar.gz
~~~~

**For another directory:**

~~~~{.shell}
johndoe@linux $ cc.py FILENAME /home/johndoe/cache/

// output in /home/johndoe/cache/ => FILENAME.tar.gz
~~~~

#------------------

#Önbellek sıkıştırıcı

Önbellek sıkıştırıcı Python ile yazıldı. Sıkıştırma metodu olarak `gzip` kullanılıyor.

#Kullanım:

**cc.py'nin bulunduğu mevcut dizin için:**

~~~~{.shell}
johndoe@linux $ cc.py -c DOSYA_ADI

// Mevcut dizine çıkacak dosya adı => DOSYA_ADI.tar.gz
~~~~

**Diğer dizinler için:**

~~~~{.shell}
johndoe@linux $ cc.py DOSYA_ADI /home/johndoe/cache/

// çıktı /home/johndoe/cache/ olarak belirtilen dizine yapılır => DOSYA_ADI.tar.gz
~~~~