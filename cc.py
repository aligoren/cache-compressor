import tarfile
from tarfile import open
from os import path
from os import getcwd
from os import unlink
from sys import argv
from sys import exit
from shutil import move
from glob import glob

# print('argv:', len(argv)) # Test for argv length

if __name__ != '__main__':
    print("Not a module!")
    exit(1)

USAGE_TEXT = 'cc.py FILE_NAME CACHE_DIRECTORY'
USAGE_CURRENT_DIRECTORY = 'cc.py -c FILE_NAME'
HELP_SHOURTCUT = 'cc.py -h'

class CacheCompressor:
    """Generate Cache Compressor Class"""
    def __init__(self):
        """Initialize sub methods"""
        self.compress
        self.ccompress

    def compress(self,filename, dir_name):
        """Get filename for output and get folder name for find cache folder. And compress with tar.gz""" 
        with tarfile.open(''.join([filename, '.tar.gz']), 'w:gz') as tar:
            tar.add(dir_name, arcname=path.basename(dir_name))
            
        fileN = ''.join([filename, '.tar.gz'])
        isThere = glob('*.tar.gz')
        if isThere:
            print('Compressed file: %s' % fileN)    
        fileSeperator = '\\'
        fileReturne = dir_name + fileSeperator + fileN
        if not path.exists(fileReturne):
            #remove(fileReturne)
            move(fileN, dir_name)
        else:
            unlink(fileReturne)
            move(fileN, dir_name)
    
    def ccompress(self, filename):
        """Get filename for output. Later make compress process in current folder"""
        with tarfile.open(''.join([filename, '.tar.gz']), 'w:gz') as tar:
            tar.add(''.join([getcwd(), '\\']), arcname=path.basename(''.join([getcwd(), '\\'])))
        isThere = glob('*.tar.gz')
        if isThere:
            print('Compressed file: %s.tar.gz' % filename)
        

cpress = CacheCompressor()

if len(argv) == 1:
    print('Help: %s' % HELP_SHOURTCUT)
elif argv[1] == '-h':
    print('Usage for Another Directory: %s' % USAGE_TEXT)
    print('\nUsage for Current Directory: %s' % USAGE_CURRENT_DIRECTORY)
elif argv[1] == '-c' and argv[2]:
    cpress.ccompress(argv[2])
elif argv[1] and argv[2]:
    cpress.compress(argv[1], argv[2])